// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAWC6F-oMypR_AJKP-BMo9NMhPlcWAl564",
    authDomain: "compras-app-5db09.firebaseapp.com",
    databaseURL: "https://compras-app-5db09.firebaseio.com",
    projectId: "compras-app-5db09",
    storageBucket: "compras-app-5db09.appspot.com",
    messagingSenderId: "790634602748",
    appId: "1:790634602748:web:6a4ce1212c85ccce92b779",
    measurementId: "G-M86TJ4KSPX"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
