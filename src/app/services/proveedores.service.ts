import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Proveedor } from '../models/proveedor.model';

@Injectable()
export class ProveedorService {
    
    presURL = 'https://compras-app-5db09.firebaseio.com/proveedores.json';
    preURL = 'https://compras-app-5db09.firebaseio.com/proveedores';
    
    constructor(private http: HttpClient) {}
    
    postProveedor( presupuesto: any) {
       return this.http.post(this.presURL,presupuesto);
    }
    

    getProveedores(){
        return this.http.get( this.presURL ).pipe( map( res => 
            {
                const proveedores:Proveedor []= [];

                Object.keys( res).forEach( key =>{

                    const proveedor:Proveedor = res[key]
                        
                    proveedor.id = key

                    proveedores.push(proveedor);
                })

                return proveedores;
            }) 
        
        );
    }

    getProveedor(id){
        const url = this.preURL+`/${id}.json`;
        console.log(id)
        return this.http.get<Proveedor>(url);
    }

    putProveedor( proveedor: any, id: string) {
        const url = this.preURL+`/${id}.json`; 
        return this.http.put(url,proveedor);
    }

    delProveedor( id: string ) {
        const url = `${ this.preURL }/${ id }.json`;
        return this.http.delete( url );
    }

    getProveedoresSearch(busqueda: string) {
        const url = `${ this.presURL }?orderBy="nombre"&startAt="${ busqueda
       }"&endAt="${ busqueda }\uf8ff"`;
        return this.http.get( url )
    }

}