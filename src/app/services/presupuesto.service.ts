import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpResponse } from '@angular/common/http';
import { map } from 'rxjs/operators'
import {Presupuesto} from '../models/presupuesto.model'

@Injectable()
export class PresupuestoService {

    presURL = 'https://compras-app-5db09.firebaseio.com/presupuestos.json';
    preURL = 'https://compras-app-5db09.firebaseio.com/presupuestos';
    
    constructor(private http: HttpClient) {}
    
    postPresupuesto( presupuesto: any) {
       return this.http.post(this.presURL,presupuesto);
    }
    

    getPresupuestos(){
        return this.http.get( this.presURL ).pipe( map( res => 
            {
                const presupuestos:Presupuesto []= [];

                Object.keys( res).forEach( key =>{

                    const presupuesto:Presupuesto = res[key]
                        
                    presupuesto.id = key

                    presupuestos.push(presupuesto);
                })

                return presupuestos;
            }) 
        
        );
    }

    getPresupuesto(id){
        const url = this.preURL+`/${id}.json`;
        console.log(id)
        return this.http.get<Presupuesto>(url);
    }

    putPresupuesto( presupuesto: any, id: string) {
        const url = this.preURL+`/${id}.json`; 
        return this.http.put(url,presupuesto);
    }

    delPresupuesto ( id: string ) {
        const url = `${ this.preURL }/${ id }.json`;
        return this.http.delete( url );
    }
}   