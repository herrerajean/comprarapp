import { Injectable } from "@angular/core";
import { AngularFireStorage } from "@angular/fire/storage";
import { AngularFirestore } from "@angular/fire/firestore";
import * as firebase from "firebase";
import { Archivo } from "../components/upload/upload.model";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";

@Injectable()
export class LoadfileService {
  private basePath: string = "uploads";
  uploads: Archivo[];

  constructor(
    private angularFirestore: AngularFireStorage,
    private db: HttpClient
  ) {}

  preUrl = `https://compras-app-5db09.firebaseio.com/uploads.json`;
  url = `https://compras-app-5db09.firebaseio.com/uploads`;

  pushUpload(upload: Archivo) {
    // console.log(firebase.storage())
    const storageRef = this.angularFirestore.storage.ref(this.basePath);
    const uploadTask = storageRef.child(`${upload.file.name}`).put(upload.file);
    uploadTask.on(
      firebase.storage.TaskEvent.STATE_CHANGED,
      snapshot => {
        // upload in progress
        upload.progress =
          (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
      },
      error => {
        // upload failed
        console.log(error);
      },
      () => {
        // upload succes
        uploadTask.snapshot.ref.getDownloadURL().then(data => {
          upload.name = upload.file.name;
          upload.url = data;
          this.db.post(this.preUrl, upload).subscribe();
        });
      }
    );
  }

  getUploads() {
    return this.db.get<Archivo[]>(this.preUrl).pipe(
      map(res => {
        const archivos: Archivo[] = [];

        Object.keys(res).forEach(key => {
          const archivo: Archivo = res[key];

          archivo.key = key;

          archivos.push(archivo);
        });

        return archivos;
      })
    );
  }

  deleteUpload(upload: Archivo) {
    this.deleteFileData(upload.key).subscribe(succes=>{
      console.log("ESTO ES EL NEXT"+succes)
    },error=>{
      console.error(error)
    },
    ()=>{
      const storageRef = this.angularFirestore.storage.ref()
      storageRef.child(`${this.basePath}/${upload.name}`).delete();
    });
  }
  private deleteFileData(key: string) {
    return this.db.delete(this.url+`/${key}.json`)
  }
}
