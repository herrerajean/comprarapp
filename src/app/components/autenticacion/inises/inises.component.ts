import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import {
  FormControl,
  FormGroup,
  FormBuilder,
  Validators
} from "@angular/forms";
import { AuthenticationService } from "src/app/services/autentication.service";

@Component({
  selector: "app-inises",
  templateUrl: "./inises.component.html",
  styleUrls: ["./inises.component.css"]
})
export class InisesComponent implements OnInit {
  loginForm: FormGroup;
  userdata: any;
  mensaje = false;

  autenticando=false;

  constructor(
    private autService: AuthenticationService,
    private router: Router,
    private activedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.loginForm = new FormGroup({
      email: new FormControl("", [Validators.required, Validators.email]),
      password: new FormControl("", [
        Validators.required,
        Validators.pattern("^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$"),
        Validators.minLength(6)
      ])
    });
  }

  onSubmit() {
    this.autenticando = true;
    this.userdata = this.saveUserdata();
    this.autService.inicioSesion(this.userdata);
    setTimeout(() => {
      if (this.isAuth() === false) {
        this.mensaje = true;
        this.autenticando = false;
      }
    }, 2000);
  }

  saveUserdata() {
    const saveUserdata = {
      email: this.loginForm.get("email").value,
      password: this.loginForm.get("password").value
    };
    return saveUserdata;
  }

  isAuth() {
    return this.autService.isAuthenticated();
  }
}
