import { Component, OnInit } from "@angular/core";
import {
  FormControl,
  FormGroup,
  FormBuilder,
  Validators
} from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { AuthenticationService } from "src/app/services/autentication.service";

@Component({
  selector: "app-registro",
  templateUrl: "./registro.component.html",
  styleUrls: ["./registro.component.css"]
})
export class RegistroComponent implements OnInit {
  registroForm: FormGroup;
  userdata: any;
  erroresForm = {
    email: "",
    password: ""
  };
  mensajesValidacion = {
    email: {
      required: "Email obligatorio",
      email: "Introduzca una dirección email correcta"
    },
    password: {
      required: "Contraseña obligatoria",
      pattern: "La contraseña debe tener al menos una letra un número ",
      minlength: "y más de 6 caracteres"
    }
  };

  constructor(
    private autService: AuthenticationService,
    private router: Router,
    private activedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.registroForm = new FormGroup({
      email: new FormControl("", [Validators.required, Validators.email]),
      password: new FormControl("", [
        Validators.required,
        Validators.pattern("^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$"),
        Validators.minLength(6)
      ])
    });
    this.registroForm.valueChanges.subscribe(data =>{
      console.log(data)
      this.onValueChanged(data)
    });
    
    this.onValueChanged();
  }

  onSubmit() {
    this.userdata = this.saveUserdata();
    this.autService.registroUsuario(this.userdata);
    this.router.navigate(["/inicio"]);
  }

  saveUserdata() {
    const saveUserdata = {
      email: this.registroForm.get("email").value,
      password: this.registroForm.get("password").value
    };
    return saveUserdata;
  }

  onValueChanged(data?: any) {
    if (!this.registroForm) {
      return;
    }
    const form = this.registroForm;
    for (const field in this.erroresForm) {
      this.erroresForm[field] = "";
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.mensajesValidacion[field];
        for (const key in control.errors) {
          this.erroresForm[field] += messages[key] + " ";
        }
      }
    }
  }
}
