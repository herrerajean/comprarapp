import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/autentication.service';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {

  constructor(private autService: AuthenticationService) { }

  ngOnInit() {
  }
  
  isAuth() {
    return this.autService.isAuthenticated();
  }

}
