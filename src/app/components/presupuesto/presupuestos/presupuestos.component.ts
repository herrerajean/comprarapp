import { Component, OnInit } from "@angular/core";
import { PresupuestoService } from "src/app/services/presupuesto.service";
import { Presupuesto } from "src/app/models/presupuesto.model";

@Component({
  selector: "app-presupuestos",
  templateUrl: "./presupuestos.component.html",
  styleUrls: ["./presupuestos.component.css"]
})
export class PresupuestosComponent implements OnInit {
  presupuestos: Presupuesto[] = [];

  constructor(private presupuestosService: PresupuestoService) {
    this.presupuestosService.getPresupuestos().subscribe(presupuestos => {
      this.presupuestos = presupuestos;
    });
  }

  ngOnInit() {}

  eliminarPresupuesto(id,index) {
    this.presupuestosService.delPresupuesto(id).subscribe(res => {
      this.presupuestos.splice(index,1);
    });
  }
}
