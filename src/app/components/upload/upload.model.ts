export class Archivo {
  key: string;
  file: File;
  name: string;
  url: string;
  progress: number;
  createdAt: Date = new Date();
  constructor(file?: File) {
    this.file = file;
    this.url="";
    this.key="";
    this.name="";
    this.url="";
    this.progress=0;
  }
}
