import { Component, OnInit } from "@angular/core";
import * as _ from "lodash";
import { Archivo } from "./upload.model";
import { LoadfileService } from "src/app/services/loadfile.service";

@Component({
  selector: "app-upload",
  templateUrl: "./upload.component.html",
  styleUrls: ["./upload.component.css"]
})
export class UploadComponent implements OnInit {
  selectedFiles: FileList;
  currentUpload: Archivo;
  loading = false;

  constructor(public loadfileService: LoadfileService) {}

  ngOnInit() {}

  uploadSingle() {
    console.error(this.selectedFiles)
    const file = this.selectedFiles.item(0);
    this.currentUpload = new Archivo(file);
    this.loading = true;
    this.loadfileService.pushUpload(this.currentUpload);
  }

  detectFiles(event){
    this.selectedFiles = event.target.files
  }
}