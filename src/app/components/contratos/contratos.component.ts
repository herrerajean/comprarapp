import { Component, OnInit } from '@angular/core';
import { FirebaseAppConfig } from '@angular/fire';
import { } from '@angular/fire/firestore';
import { Archivo } from '../upload/upload.model';
import { LoadfileService } from 'src/app/services/loadfile.service';

@Component({
  selector: 'app-contratos',
  templateUrl: './contratos.component.html',
  styleUrls: ['./contratos.component.css']
})
export class ContratosComponent implements OnInit {

  uploads: any[]= [];

  constructor(private loadfileService: LoadfileService) {
    console.log(this.uploads)
    this.loadfileService.getUploads().subscribe(data =>{
      this.uploads = data;
    })
  }

  ngOnInit() {
    
  }

}
