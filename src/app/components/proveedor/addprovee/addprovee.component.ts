import { Component, OnInit, ViewChildren, ViewChild } from '@angular/core';

import { NgForm } from '@angular/forms';
import { ProveedorService } from 'src/app/services/proveedores.service';

@Component({
  selector: 'app-addprovee',
  templateUrl: './addprovee.component.html',
  styleUrls: ['./addprovee.component.css']
})
export class AddproveeComponent implements OnInit {
  
  
  proveedor: any;
  @ViewChild('formpro', {static: false}) formpro:NgForm;

  provincias: string[] = [ 'Ate',
  'Barranco',
  'Bellavista',
  'Breña',
  'Callao',
  'Carmen de la Legua',
  'Comas',
  'Chorrillos',
  'El Agustino',
  'Independencia',
  'Jesús María',
  'La Molina',
  'La Perla',
  'La Punta',
  'La Victoria',
  'Lince',
  'Los Olivos',
  'Magdalena del Mar',
  'Miraflores',
  'Pueblo Libre',
  'Puente Piedra',
  'Rimac',
  'Santa Anita',
  'San Borja',
  'San Isidro',
  'San Juan de Miraflores',
  'San Luis',
  'San Martin de Porres',
  'San Miguel',
  'San Juan de Lurigancho',
  'Santa Rosa',
  'Santiago de Surco',
  'Surquillo',
  'Ventanilla',
  'Villa El Savador',
  'Villa María del Triunfo']

  constructor(private proveedorService:ProveedorService) { 
    this.proveedor = {
      nombre: '',
      cif: '',
      direccion: '',
      cpostal: '',
      localidad: '',
      distrito:'',
      telefono: null,
      email: '',
      contacto: ''
    }
  }

  ngOnInit() {
  }
  
  onSubmit(){

    this.proveedor.nombre = this.formpro.value.nombre;
    this.proveedor.cif = this.formpro.value.cif;
    this.proveedor.direccion = this.formpro.value.direccion;
    this.proveedor.cpostal = this.formpro.value.cp;
    this.proveedor.localidad = this.formpro.value.localidad;
    this.proveedor.distrito = this.formpro.value.provincia;
    this.proveedor.telefono = this.formpro.value.telefono;
    this.proveedor.email = this.formpro.value.email;
    this.proveedor.contacto = this.formpro.value.contacto;

    this.proveedorService.postProveedor(this.proveedor).subscribe();
    this.formpro.reset();
  }

}
