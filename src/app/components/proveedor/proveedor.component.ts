import { Component, OnInit } from "@angular/core";
import { ProveedorService } from "src/app/services/proveedores.service";
import { Proveedor } from "src/app/models/proveedor.model";
import {
  FormControl,
  FormGroup,
  FormBuilder,
  Validators
} from "@angular/forms";

@Component({
  selector: "app-proveedor",
  templateUrl: "./proveedor.component.html",
  styleUrls: ["./proveedor.component.css"]
})
export class ProveedorComponent implements OnInit {
  proveedores: any[] = [];

  campoBusqueda: FormControl;
  busqueda: string;

  cargando = false;
  resultados = false;
  noresultados = false;

  constructor(private proveedorService: ProveedorService) {
    this.proveedorService.getProveedores().subscribe(data => {
      this.proveedores = data;
      this.cargando = false;
    });
  }

  ngOnInit() {
    this.campoBusqueda = new FormControl();
    this.campoBusqueda.valueChanges.subscribe(term => {
      this.busqueda = term;
      this.cargando = true;
      if (this.busqueda.length !== 0) {
        this.proveedorService
          .getProveedoresSearch(this.busqueda)
          .subscribe(proveedores => {
            this.proveedores = [];
            for (const id$ in proveedores) {
              const p = proveedores[id$];
              p.id$ = id$;
              this.proveedores.push(proveedores[id$]);
            }
            if (this.proveedores.length < 1 && this.busqueda.length >= 1) {
              this.noresultados = true;
            } else {
              this.noresultados = false;
            }
          });
        this.cargando = false;
        this.resultados = true;
      } else {
        this.proveedores = [];
        this.cargando = false;
        this.resultados = false;
      }
    });
  }
}
