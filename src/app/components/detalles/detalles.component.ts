import { Component, OnInit,Input } from '@angular/core';
import { Archivo } from '../upload/upload.model';
import { LoadfileService } from 'src/app/services/loadfile.service';

@Component({
  selector: 'app-detalles',
  templateUrl: './detalles.component.html',
  styleUrls: ['./detalles.component.css']
})
export class DetallesComponent implements OnInit {
  
  @Input() upload:Archivo;

  constructor(private loadfileService: LoadfileService) { }

  ngOnInit() {
  }

  deleteUpload(upload) {
    this.loadfileService.deleteUpload(this.upload);
  }


}
