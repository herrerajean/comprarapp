import {Routes} from '@angular/router'
import { InicioComponent } from './components/inicio/inicio.component'
import { ProveedorComponent } from './components/proveedor/proveedor.component'
import { AddproveeComponent } from './components/proveedor/addprovee/addprovee.component'
import { AddpresComponent } from './components/presupuesto/addpres/addpres.component'
import { PresupuestosComponent } from './components/presupuesto/presupuestos/presupuestos.component'
import { EditpressComponent } from './components/presupuesto/editpress/editpress.component'
import { RegistroComponent } from './components/autenticacion/registro/registro.component'
import { InisesComponent } from './components/autenticacion/inises/inises.component'
import { GuardService } from './services/guard.service'
import { FacturasComponent } from './facturas/facturas/facturas.component'
import { UploadComponent } from './components/upload/upload.component'
import { ContratosComponent } from './components/contratos/contratos.component'

export const APP_ROUTES:Routes = [
    {path:'',component: InicioComponent},
    {path:'proveedores',component:ProveedorComponent,canActivate:[GuardService]},
    {path:'addprovee',component:AddproveeComponent,canActivate:[GuardService]},
    {path: 'addpres', component: AddpresComponent,canActivate:[GuardService]},
    {path: 'presupuestos', component: PresupuestosComponent,canActivate:[GuardService]},
    {path: 'editpres/:id', component: EditpressComponent,canActivate:[GuardService]},
    {path: 'registro', component: RegistroComponent },
    {path: 'uploads', component: UploadComponent },
    {path: 'iniciosesion', component: InisesComponent },
    {path: 'facturas', component: FacturasComponent },
    {path: 'contratos', component: ContratosComponent },
    {path:'**',component:InicioComponent}
]