import { Component, OnInit } from "@angular/core";
import * as firebase from "firebase";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent implements OnInit {
  title = "ComprasApp";

  ngOnInit() {
    firebase.initializeApp({
      apiKey: "AIzaSyAWC6F-oMypR_AJKP-BMo9NMhPlcWAl564",
      authDomain: "compras-app-5db09.firebaseapp.com"
    });
  }
}
