import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {RouterModule} from '@angular/router'

import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { ProveedorComponent } from './components/proveedor/proveedor.component';
import { InicioComponent } from './components/inicio/inicio.component';
import { APP_ROUTES } from './app.route';
import { HeaderComponent } from './components/header/header.component';
import { AddproveeComponent } from './components/proveedor/addprovee/addprovee.component';
import { AddpresComponent } from './components/presupuesto/addpres/addpres.component';
import { PresupuestoService } from './services/presupuesto.service';
import { PresupuestosComponent } from './components/presupuesto/presupuestos/presupuestos.component';
import { EditpressComponent } from './components/presupuesto/editpress/editpress.component';
import { RegistroComponent } from './components/autenticacion/registro/registro.component';
import { AuthenticationService } from './services/autentication.service';
import { InisesComponent } from './components/autenticacion/inises/inises.component';
import { GuardService } from './services/guard.service';
import { FacturasModule } from './facturas/facturas.module';
import { ProveedorService } from './services/proveedores.service';

import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { LoadfileService } from './services/loadfile.service';
import { UploadComponent } from './components/upload/upload.component';
import { environment } from 'src/environments/environment';
import { ContratosComponent } from './components/contratos/contratos.component';
import { DetallesComponent } from './components/detalles/detalles.component';

@NgModule({
  declarations: [
    AppComponent,
    ProveedorComponent,
    InicioComponent,
    HeaderComponent,
    AddproveeComponent,
    AddpresComponent,
    PresupuestosComponent,
    EditpressComponent,
    RegistroComponent,
    InisesComponent,
    UploadComponent,
    ContratosComponent,
    DetallesComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(APP_ROUTES),
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    FacturasModule,
    AngularFireModule.initializeApp(environment.firebase,'compras-app-5db09'),
    AngularFirestoreModule, // imports firebase/firestore, only needed for database features
    AngularFireStorageModule,
    AngularFireAuthModule
  ],
  providers: [ProveedorService,PresupuestoService,AuthenticationService,GuardService,LoadfileService],
  bootstrap: [AppComponent]
})
export class AppModule { }
